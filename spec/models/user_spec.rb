require 'rails_helper'

RSpec.describe User, type: :model do
  describe 'Validations' do
    it  {is_expected.to validate_presence_of(:name)}
    it  {is_expected.to validate_presence_of(:email)}
  end 

  describe 'Associations' do
    it { is_expected.to have_many(:microposts) }
  end

end
