require 'rails_helper'

RSpec.feature "Create new user", :type => :feature do
    scenario "Redirect to page with name and email of user created" do 
        user_name = "Test User"
        user_email = "test@email.com"

        visit "users" 
        click_on "New User"

        fill_in "user_name", with: user_name
        fill_in "user_email", with: user_email

        click_on "Create User"

        expect(page).to have_text(user_name, user_email)
    end
end